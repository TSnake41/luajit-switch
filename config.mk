# LuaJIT Switch configuration file

# Default flags
LJ_CFLAGS := -DLUAJIT_TARGET=LUAJIT_ARCH_ARM64 -DLUAJIT_OS=LUAJIT_OS_OTHER \
	-DLUAJIT_USE_SYSMALLOC -DLUAJIT_ENABLE_GC64 -DLJ_ABI_SOFTFP=0

DASM_FLAGS := -D ENDIAN_LE -D P64

# Lua interpreter to use to build vm (must support bit.* functions)
LUA := luajit

#
# User-settings, you can change these
#
# When you have a Enable/Disable, you either have to uncomment
# the "Enable" part and comment the "Disable" part or vice-versa.
#
# DO NOT KEEP BOTH COMMENTED OR UNCOMMENTED.
#

#
# Disable unwinding
#
#DASM_FLAGS += -D NO_UNWIND
#LJ_CFLAGS += -DLUAJIT_NO_UNWIND=1

#
# Enable/Disable JIT
#

# Enable JIT
#DASM_FLAGS += -D JIT

# Disable JIT
LJ_CFLAGS += -DLUAJIT_DISABLE_JIT

#
# Enable/Disable FFI
#

# Enable FFI
DASM_FLAGS += -D FFI

# Disable FFI
#LJ_CFLAGS += -DLUAJIT_DISABLE_FFI

#
# Enable SLDL
#
SLDL_ENABLE = 1
