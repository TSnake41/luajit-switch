include config.mk

CFLAGS := $(LJ_CFLAGS)

# Directories
LJ_DIR := ./LuaJIT
LJ_SRC=$(LJ_DIR)/src

DASM_DIR := $(LJ_DIR)/dynasm
DASM := $(LUA) $(DASM_DIR)/dynasm.lua

DASC := $(LJ_SRC)/vm_arm64.dasc

DKP_DIR := ./dkp
DKP_SRC := $(DKP_DIR)/source
DKP_INC := $(DKP_DIR)/include

BUILDVM := ./buildvm

# LuaJIT libs to build VM
ALL_LIB := $(LJ_SRC)/lib_base.c $(LJ_SRC)/lib_math.c $(LJ_SRC)/lib_bit.c \
	$(LJ_SRC)/lib_string.c $(LJ_SRC)/lib_table.c $(LJ_SRC)/lib_io.c $(LJ_SRC)/lib_os.c \
	$(LJ_SRC)/lib_package.c $(LJ_SRC)/lib_debug.c $(LJ_SRC)/lib_jit.c $(LJ_SRC)/lib_ffi.c

all: src

bin: src
	make -C $(DKP_DIR)
	mkdir -p $@
	cp -r $(DKP_DIR)/include $(DKP_DIR)/lib $@/

clean:
	rm -rf $(DKP_SRC) $(DKP_INC) $(DKP_DIR)/release $(DKP_DIR)/lib $(DKP_DIR)/debug || true
	rm -rf buildvm* $(LJ_SRC)/host/buildvm_arch.h dkp/Makefile || true

src: dkp/Makefile lj_vm lj_inc
	cp $(LJ_SRC)/lib_*.c $(DKP_SRC)/
	cp $(LJ_SRC)/lj_* $(DKP_SRC)/
ifeq ($(SLDL_ENABLE),1)
	# sldl support
	cp sldl/*.c $(DKP_SRC)/
	cp sldl/*.h $(DKP_INC)/
	# patch lj_arch.h to force dlopen on non-POSIX platforms
	echo -e "\n#undef LJ_TARGET_DLOPEN\n#define LJ_TARGET_DLOPEN 1\n\n" >> $(DKP_SRC)/lj_arch.h
endif


lj_inc: $(LJ_SRC)/luajit.h $(LJ_SRC)/lua.h $(LJ_SRC)/lua.hpp $(LJ_SRC)/lauxlib.h $(LJ_SRC)/lualib.h $(LJ_SRC)/luaconf.h
	mkdir -p $(DKP_INC)
	cp $^ $(DKP_DIR)/include/

lj_vm: buildvm
	mkdir -p $(DKP_SRC)
	$(BUILDVM) -m elfasm -o $(DKP_SRC)/lj_vm.s
	$(BUILDVM) -m bcdef -o $(DKP_SRC)/lj_bcdef.h $(ALL_LIB)
	$(BUILDVM) -m ffdef -o $(DKP_SRC)/lj_ffdef.h $(ALL_LIB)
	$(BUILDVM) -m libdef -o $(DKP_SRC)/lj_libdef.h $(ALL_LIB)
	$(BUILDVM) -m recdef -o $(DKP_SRC)/lj_recdef.h $(ALL_LIB)
	# $(BUILDVM) -m vmdef -o $(DKP_SRC)/jit/vmdef.lua $(ALL_LIB)
	$(BUILDVM) -m folddef -o $(DKP_SRC)/lj_folddef.h $(LJ_SRC)/lj_opt_fold.c

buildvm: $(LJ_SRC)/host/buildvm_arch.h
	$(CC) -I$(LJ_SRC) -I$(DASM_DIR) $(CFLAGS) $(LJ_SRC)/host/buildvm*.c -o buildvm

$(LJ_SRC)/host/buildvm_arch.h:
	$(DASM) $(DASM_FLAGS) -o $@ $(DASC)

dkp/Makefile: dkp/Makefile.template
	cat config.mk dkp/Makefile.template > $@ 

.PHONY: all bin clean src lj_vm lj_inc buildvm
